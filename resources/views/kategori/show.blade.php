@extends('layout.master')

@section('judul')
Detail Kategori
@endsection

@section('content')

<h1>{{$kategori->nama}}</h1>
<p>{{$kategori->deskripsi}}</p>

<div class="row">
    @foreach ($kategori->berita as $item)        
        <div class="col-4">
            <div class="card">
                <img src="{{asset('gambar/'. $item->thumbnail)}}" class="card-img-top" alt="...">
                <div class="card-body">
                <h3>{{$item->judul}}</h3>
                <p class="card-text">
                    {{$item->content}}
                </p>
                </div>
            </div>
        </div>
    @endforeach
</div>

@endsection