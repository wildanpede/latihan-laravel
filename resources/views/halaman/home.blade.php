@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$first_name}} {{$last_name}}</h1>
    <p><strong>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</strong></p>
@endsection