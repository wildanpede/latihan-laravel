@extends('layout.master')

@section('judul')
Buat Account Baru
@endsection

@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="language_spoken">Bahasa Indonesia <br>
        <input type="checkbox" name="language_spoken">English <br>
        <input type="checkbox" name="language_spoken">Other <br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection