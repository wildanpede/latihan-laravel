<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('register', 'AuthController@bio');

Route::post('welcome', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::group(['middleware' => ['auth']], function() {
    // CRUD Kategori
    // Create
    Route::get('/kategori/create', 'KategoriController@create'); //Route menuju form create
    Route::post('/kategori', 'KategoriController@store'); //Route untuk menyimpan data ke database

    // Read
    Route::get('/kategori', 'KategoriController@index'); //Route list kategori
    Route::get('/kategori/{kategori_id}', 'KategoriController@show'); //Route detail kategori

    // Update
    Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit'); //Route menuju ke form edit
    Route::put('/kategori/{kategori_id}', 'KategoriController@update'); //Route untuk update data berdasarkan id di database

    // Delete
    Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy'); //Route untuk hapus data di database

    // Update Profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('komentar', 'KomentarController')->only([
        'index', 'store'
    ]);
});

// CRUD Berita
Route::resource('berita', 'BeritaController');
Auth::routes();
