<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language_spoken = $request['language_spoken'];
        $bio = $request['bio'];
        return view('halaman.home', compact('first_name', 'last_name', 'gender', 'nationality', 'language_spoken', 'bio'));
    }
}
